from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, ForeignKey
import random
import re

def generator_imienia():
    imiona_meskie = list(open('meskie', 'r'))
    imiona_meskie = sorted(imiona_meskie)
    liczba = random.randrange(0, len(imiona_meskie))
    imie = imiona_meskie[liczba].capitalize()
    imie = imie.replace('\n', '')
    return imie

def generator_nazwiska():
    nazwiska = list(open('nazwiska', 'r'))
    nazwiska = sorted(nazwiska)
    liczba = random.randrange(0, len(nazwiska))
    nazwisko = nazwiska[liczba].capitalize()
    nazwisko = nazwisko.replace('\n', '')
    return nazwisko

def generator_pesel(plec, data_urodzenia):
    pesel = str(data_urodzenia[8:10]) + str(data_urodzenia[3:5]) + str(data_urodzenia[0:2]) + str(random.randrange(100,1000))
    if plec == 'm':
        odd_rand_num = random.randrange(1, 10, 2)
        pesel = pesel + str(odd_rand_num)
    else:
        even_rand_num = random.randrange(0, 10, 2)
        pesel = pesel + str(even_rand_num)
    pesel = pesel + str(random.randrange(0,10))
    return pesel

def generator_plci():
        return 'm'

def generator_daty_urodzenia():
    data_urodzenia = ''
    miesiac = random.randrange(1,13)
    if miesiac < 10:
        miesiac = '0' + str(miesiac)

    if miesiac == 1 | 3 | 5 | 7 | 8 | 10 | 12:
        dzien = random.randrange(1,32)
        if dzien < 10:
            dzien = '0' + str(dzien)
    elif miesiac == 4 | 6 | 9 | 11:
        dzien = random.randrange(1,31)
        if dzien < 10:
            dzien = '0' + str(dzien)
    else:
        dzien = random.randrange(1,30)
        if dzien < 10:
            dzien = '0' + str(dzien)

    rok = random.randrange(1900, 2022)
    data_urodzenia = str(dzien) + '.' + str(miesiac) + '.' + str(rok)
    return data_urodzenia

def generator_miasta():
    miasta = list(open('miasta', 'r'))
    miasta = sorted(miasta)
    liczba = random.randrange(0, len(miasta))
    miasto = miasta[liczba].replace('\n', '')
    return miasto

def generator_ulicy():
    ulice = list(open('ulice', 'r'))
    ulice = sorted(ulice)
    liczba = random.randrange(0, len(ulice))
    ulica = ulice[liczba].replace('\n', '')
    return ulica[4:]

def generator_wojewodztwa():
    wojewodztwa = list(open('wojewodztwa', 'r'))
    wojewodztwa = sorted(wojewodztwa)
    liczba = random.randrange(0, len(wojewodztwa))
    wojewodztwo = wojewodztwa[liczba].replace('\n', '')
    return wojewodztwo.capitalize()

def generator_numeru_domu():
    numer_domu = str(random.randrange(0, 200))
    return numer_domu

def losowa_liczba():
    liczba = str(random.randrange(0, 10))
    return liczba

def generator_kodu_pocztowego():
    kod_pocztowy = losowa_liczba() + losowa_liczba() + '-' + losowa_liczba() + losowa_liczba() + losowa_liczba()
    return kod_pocztowy

def generator_numeru_telefonu():
    numer_telefonu = []
    for i in range(0,9):
        liczba = random.randrange(0,10)
        numer_telefonu.append(str(liczba))
    numer_telefonu = ''.join(numer_telefonu)
    return numer_telefonu

def generator_email(imie, nazwisko):
    email = str(imie) + '.' + str(nazwisko) + '@gmail.com'
    return email

class Osoba:
    imie = generator_imienia()
    nazwisko = generator_nazwiska()
    plec = generator_plci()
    data_urodzenia = generator_daty_urodzenia()
    pesel = generator_pesel(plec, data_urodzenia)

    miasto = generator_miasta()
    ulica = generator_ulicy()
    numer = generator_numeru_domu()
    #numer_mieszkania = generator_numeru_mieszkania()
    wojewodztwo = generator_wojewodztwa()
    kod_pocztowy = generator_kodu_pocztowego()

    numer_telefonu = generator_numeru_telefonu()
    email = generator_email(imie, nazwisko)

    def drukuj(self):
        print(str(self.imie))
        print(self.nazwisko)
        print(self.plec)
        print(self.data_urodzenia)
        print(self.pesel)
        print(self.miasto)
        print(self.ulica)
        print(self.numer)
        print(self.wojewodztwo)
        print(self.kod_pocztowy)
        print(self.numer_telefonu)
        print(self.email)
def create_database():
    engine = create_engine('sqlite:///database.db', echo=True)
    meta = MetaData()

    osoby = Table(
        'osoby', meta,
        Column('id', Integer, primary_key=True),
        Column('imie', String, nullable=False),
        Column('nazwisko', String, nullable=False),
        Column('pesel', String(11), nullable=False),
        Column('plec', String(1), nullable=False),
        Column('data_urodzenia', String(1), nullable=False),
        Column('adres_id_', Integer, ForeignKey("adresy.id"), nullable=False),
        Column('kontakt_id_', Integer, ForeignKey("kontakty.id"), nullable=False),
    )

    adresy = Table(
        'adresy', meta,
        Column('id', Integer, primary_key=True),
        Column('miasto', String, nullable=False),
        Column('ulica', String, nullable=False),
        Column('numer ', String, nullable=False),
        Column('numer_mieszkania', String),
        Column('wojewodztwo', String, nullable=False),
        Column('kod_pocztowy', String, nullable=False),
        Column('panstwo', String, nullable=False),
    )

    kontakty = Table(
        'kontakty', meta,
        Column('id', Integer, primary_key=True),
        Column('numer_telefonu', String, nullablodd_rand_nume=False),
        Column('email', String, nullable=False),
    )

    meta.create_all(engine)


print('MENU')
print('1 - Utwórz bazę danych')
print('2 - Utwórz fikcyjną osobę')
print('3 - dodaj osobę do bazy danych')

wybor = input()
if wybor == '1':
    create_database()
elif wybor == '2':
    fikcyjna_osoba = Osoba()
    fikcyjna_osoba.drukuj()
elif wybor == '3':
    print('Dodaj osobe do bazy')
else:
    print('Nie ma takiej funkcji')